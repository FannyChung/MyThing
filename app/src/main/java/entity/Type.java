package entity;

import java.io.Serializable;

/**
 * 物品类型
 * Created by fang on 2016/7/23 0023.
 */
public class Type implements Serializable {
    public Type(int code) {
        this.code = code;
    }

    int code;
    String name;
    int parentCd;
    String parentName;

    public Type(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public Type(int code, String name, int parentCd, String parentName) {
        this.code = code;
        this.name = name;
        this.parentCd = parentCd;
        this.parentName = parentName;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParentCd() {
        return parentCd;
    }

    public void setParentCd(int parentCd) {
        this.parentCd = parentCd;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }
}
