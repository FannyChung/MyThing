package entity.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import entity.Place;
import entity.Thing;
import entity.Type;

/**
 * Created by fang on 2016/8/7 0007.
 */
public class ThingDAO {
    private  DBOpenHelper helper;
    private SQLiteDatabase db;

    public ThingDAO(Context context) {
        helper=new DBOpenHelper(context);
    }

    public void add(Thing thing){
        db=helper.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put("name",thing.getName());
        values.put("count",thing.getCount());
        values.put("price",thing.getPrice());
        values.put("sublist",thing.getSubList());
        values.put("placeId",thing.getPlace().getCode());
        values.put("placeName",thing.getPlace().getName());
        values.put("typeId",thing.getType().getCode());
        values.put("typeName",thing.getType().getName());
        values.put("adtInfo",thing.getAdtInfo());
        db.insert("t_thing",null,values);
    }

    public void update(Thing thing){
        db=helper.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put("name",thing.getName());
        values.put("count",thing.getCount());
        values.put("price",thing.getPrice());
        values.put("sublist",thing.getSubList());
        values.put("placeId",thing.getPlace().getCode());
        values.put("placeName",thing.getPlace().getName());
        values.put("typeId",thing.getType().getCode());
        values.put("typeName",thing.getType().getName());
        values.put("adtInfo",thing.getAdtInfo());
        db.update("t_thing", values, "sid=?", new String[]{String.valueOf(thing.getCodeNumber())});
    }

    /**删除对应id的物品
     * @param sid
     */
    public void delete(long sid){
        db=helper.getWritableDatabase();
        db.delete("t_thing", "sid=?", new String[]{String.valueOf(sid)});
    }

    /**查找该id对应的物品
     * @param sid 物品id
     * @return
     */
    public  Thing find(long sid){
        Thing thing=null;
        db=helper.getReadableDatabase();
        Cursor cursor= db.query("t_thing",new String[]{"sid","name","count","price","sublist","adtInfo","typeId","placeId","typeName","placeName"},"sid=?",new String[]{String.valueOf(sid)},null,null,null);
        if(cursor.moveToNext()){
            thing.setCodeNumber(cursor.getInt(0));
            thing.setAdtInfo(cursor.getString(7));
            thing.setCount(cursor.getInt(2));
            thing.setName(cursor.getString(1));
            int placeId=cursor.getInt(5);
            int typeId=cursor.getInt(6);
            entity.Place p=new Place(placeId,cursor.getString(8));
            Type tp=new Type(typeId,cursor.getString(9));
            thing.setPlace(p);
            thing.setType(tp);
            thing.setPrice(cursor.getInt(3));
            thing.setSubList(cursor.getString(4));
            //int codeNumber, String name, int count, int price, String subList, Place place, Type type, String adtInf
        }
        return thing;
    }


    /**按照类型或位置查找所有满足条件的物品
     * @param type 查询条件，不查询时为编号-1
     * @param place 查询；不查询时为编号-1
     * @return 满足条件的所有物品
     */
    public  ArrayList<Thing> findAll(Type type,Place place){
        ArrayList<Thing> things=new ArrayList<Thing>();

        db=helper.getReadableDatabase();
        Cursor cursor=null;
        int typeId=type.getCode();
        int placeId=place.getCode();
        if(typeId==-1&&placeId==-1){
            return things;
        }else if(placeId!=-1&typeId==-1) {//用地点查询
            cursor= db.query("t_thing", new String[]{"sid", "name", "count", "price", "sublist", "adtInfo", "typeId", "placeId","typeName","placeName"}, "placeId=?", new String[]{String.valueOf(placeId)}, null, null, null);
        }else if ( placeId==-1&&typeId!=-1){//用类型查询
            cursor=db.query("t_thing", new String[]{"sid", "name", "count", "price", "sublist", "adtInfo", "typeId", "placeId","typeName","placeName"}, "typeId=?", new String[]{String.valueOf(typeId)}, null, null, null);
        }else  {
            cursor=db.query("t_thing", new String[]{"sid", "name", "count", "price", "sublist", "adtInfo", "typeId", "placeId","typeName","placeName"}, "typeId=? and placeId=?", new String[]{String.valueOf(typeId),String.valueOf(placeId)}, null, null, null);
        }
        while (cursor.moveToNext()){
            Thing thing=null;
            thing.setCodeNumber(cursor.getInt(0));
            thing.setAdtInfo(cursor.getString(7));
            thing.setCount(cursor.getInt(2));
            thing.setName(cursor.getString(1));
            if(placeId==-1) {
                placeId=cursor.getInt(5);
                Place p=new Place(placeId,cursor.getString(8));
                thing.setPlace(p);
            }else  {
                thing.setPlace(place);
            }
            if(typeId==-1){
                typeId=cursor.getInt(6);
                Type tp=new Type(typeId,cursor.getString(9));
                thing.setType(tp);
            }else {
                thing.setType(type);
            }
            thing.setPrice(cursor.getInt(3));
            thing.setSubList(cursor.getString(4));

            things.add(thing);
        }
        return  things;
    }

    /**删除多个id对应的数据项
     * @param sids
     */
    public void delete(Integer...sids){
        if(sids.length>0){
            StringBuffer sb=new StringBuffer();
            String[] strID=new String[sids.length];
            for (int i = 0; i < sids.length; i++) {
                sb.append('?').append(',');
                strID[i]=String.valueOf(sids[i]);
            }
            sb.deleteCharAt(sb.length()-1);
            db=helper.getWritableDatabase();
            db.delete("t_thing","sid in ("+sb+")",strID);
        }
    }
}
