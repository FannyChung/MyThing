package entity.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by fang on 2016/8/7 0007.
 */
public class DBOpenHelper extends SQLiteOpenHelper{
    private  static  final  int VERSION=1;
    private  static  final  String DBNAME ="thisDb.db";
    private  static  final  String create_thing_table="create table t_thing (sid integer primary key autoincrement,name varchar(20),count integer,price integer,sublist varchar(20),placeId integer,typeId integer,adtInfo varchar(20),placeName varchar(20),typeName varchar(20))";
    private  static  final  String Create_type_table="create table t_type(sid integer primary key autoincrement,name varchar(20) unique, pid integer, pname varchar(20),)";
    private  static  final  String Create_place_table="create table t_place(sid integer primary key autoincrement,name varchar(20) unique, pid integer, pname varchar(20),)";
    //档
    public DBOpenHelper(Context context) {
        super(context, DBNAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(create_thing_table);
        db.execSQL(Create_type_table);
        db.execSQL(Create_place_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO
    }
}
