package entity;

import java.io.Serializable;

/**
 * Created by fang on 2016/7/23 0023.
 */
public class Thing implements Serializable{
    /**
     * id
     */
    int codeNumber;

    public Thing(String name, int count, int price, String subList, Place place, Type type, String adtInfo) {
        this.name = name;
        this.count = count;
        this.price = price;
        this.subList = subList;
        this.place = place;
        this.type = type;
        this.adtInfo = adtInfo;
    }

    public Thing(int codeNumber, String name, int count, int price, String subList, Place place, Type type, String adtInfo) {
        this.codeNumber = codeNumber;
        this.name = name;
        this.count = count;
        this.price = price;
        this.subList = subList;
        this.place = place;
        this.type = type;
        this.adtInfo = adtInfo;
    }

    /**
     *物品名称
     */
    String name;
    /**
     * 件数
     */
    int count;

    /**
     * 价格
     */
    int price;
    /**
     * 子物品
     */
    String subList;
    /**
     * 所在位置
     */
    Place place;
    /**
     * 类型
     */
    Type type;


    /**
     * 备注
     */
    String adtInfo;

    public Thing(int codeNumber, String name, int count, Place place, Type type) {
        this.codeNumber = codeNumber;
        this.name = name;
        this.count = count;
        this.place = place;
        this.type = type;
    }

    public String getAdtInfo() {
        return adtInfo;
    }

    public void setAdtInfo(String adtInfo) {
        this.adtInfo = adtInfo;
    }
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getSubList() {
        return subList;
    }

    public void setSubList(String subList) {
        this.subList = subList;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setCodeNumber(int codeNumber) {
        this.codeNumber = codeNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public int getCodeNumber() {
        return codeNumber;
    }

    public int getCount() {
        return count;
    }
}
