package entity;

import java.io.Serializable;

/**
 * 位置
 * Created by fang on 2016/7/23 0023.
 */
public class Place implements Serializable {
    public Place(int code) {
        this.code = code;
    }

    int code;
    String name;
    int parentCode;
    String parentName;

    public Place(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public Place(int code, String name, int parentCode, String parentName) {
        this.code = code;
        this.name = name;
        this.parentCode = parentCode;
        this.parentName = parentName;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParentCode() {
        return parentCode;
    }

    public void setParentCode(int parentCode) {
        this.parentCode = parentCode;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }
}
