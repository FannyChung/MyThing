package entity;

import android.app.Application;

import java.util.ArrayList;

import entity.DAO.ThingDAO;
import entity.Place;
import entity.Thing;
import entity.Type;

/**管理公用的参数
 * Created by fang on 2016/9/11 0011.
 */
public class MyApplication extends Application {

    public static ArrayList<Thing> allThings=new ArrayList<Thing>() ;
    public static ArrayList<Place> allPlace=new ArrayList<Place>();
    public static ArrayList<Type> allType=new ArrayList<Type>();

    /**
     * 从数据库读取并初始化
     */
    private void readFromDB() {
        ThingDAO thingDao=new ThingDAO(this);
        this.allThings=thingDao.findAll(new Type(-1),new Place(-1));
    }

}
