package com.example.fang.mything;

import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

import entity.DAO.ThingDAO;
import entity.MyApplication;
import entity.Place;
import entity.Thing;
import entity.Type;

public class EditThingActivity extends AppCompatActivity implements View.OnClickListener {

    private Thing currentThing;
    private Type currentType;
    private Place currentPlace;
    private  boolean isAdded;
    EditText textName=(EditText)findViewById(R.id.editText);
    EditText textAdtInfo=(EditText)findViewById(R.id.editText3);
    NumberPicker numberPicker=(NumberPicker)findViewById(R.id.numberPicker);
    EditText txtPrice=(EditText)findViewById(R.id.editText2);
    EditText txtSubThing=(EditText)findViewById(R.id.editText5);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_thing);
        Intent intent=getIntent();
        if(intent.getSerializableExtra("thing")!=null) {
            currentThing = (Thing) intent.getSerializableExtra("thing");
            this.displayThing();
            this.isAdded=false;
        }else {
            this.isAdded=true;
        }
    }

    private void displayThing() {
        textName.setText(currentThing.getName());
        textAdtInfo.setText(currentThing.getAdtInfo());
        numberPicker.setValue(currentThing.getCount());
        txtPrice.setText(currentThing.getPrice());
        txtSubThing.setText(currentThing.getSubList());
    }


    /**
     * 启动，用来编辑该对象
     * @param context
     * @param thing
     */
    public  static  void actionStart(Context context, Thing thing){
        Intent intent=new Intent(context,EditThingActivity.class);
        intent.putExtra("thing", (Serializable) thing);
        context.startActivity(intent);

    }


    public  static  void actionStart(Context context){
        Intent intent=new Intent(context,EditThingActivity.class);
        context.startActivity(intent);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textView14://显示要选择的地点
               this.listShow(R.id.textView14);
                break;
            case  R.id.textView15://显示要选择的类型
                this.listShow(R.id.textView15);
                break;
            case R.id.button://删除
                if(!this.isAdded)//如果是修改，才进行删除
                    new ThingDAO(this).delete(currentThing.getCodeNumber());
                ( (MyApplication)getApplication()).allThings.remove(currentThing);
                break;
            case R.id.button2://保存
                if(this.isAdded) {//增加
                    currentThing=new Thing(textName.getText().toString(),numberPicker.getValue(),Integer.parseInt(txtPrice.getText().toString()),txtSubThing.getText().toString(),currentPlace,currentType,textAdtInfo.getText().toString());
                    new ThingDAO(this).add(currentThing);
                    ( (MyApplication)getApplication()).allThings.add(currentThing);
                }else{//修改
                    ( (MyApplication)getApplication()).allThings.remove(currentThing);
                    int code=currentThing.getCodeNumber();
                    currentThing=new Thing(code,textName.getText().toString(),numberPicker.getValue(),Integer.parseInt(txtPrice.getText().toString()),txtSubThing.getText().toString(),currentPlace,currentType,textAdtInfo.getText().toString());
                    new ThingDAO(this).update(currentThing);
                    ( (MyApplication)getApplication()).allThings.add(currentThing);
                }
        }
    }

    private String listShow(int kindID){
        String reslut="";
        String title="";
        ArrayList<String> listStr=new ArrayList<String>();
        switch (kindID){
            case R.id.textView14:
                title="选择地点";
                //读取所有地点的string，并存到listStr
                for (Place p : ( (MyApplication)getApplication()).allPlace ) {
                    listStr.add(p.getName());
                }
                break;
            case R.id.textView15:
                title="选择类型";
                //读取所有类型的string，并存到listStr
                for (Type p : ( (MyApplication)getApplication()).allType ) {
                    listStr.add(p.getName());
                }
                break;
        }
        new AlertDialog.Builder(EditThingActivity.this) // build AlertDialog
                .setTitle(title) // title
                .setItems((String[])listStr.toArray(), new DialogInterface.OnClickListener() { //content
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss(); //关闭alertDialog
                    }
                })
                .show();
        return  reslut;
    }


}
